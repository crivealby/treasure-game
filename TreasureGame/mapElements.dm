mob/var/list/grid

mob
	Player
		icon = 'Player.dmi'
		icon_state = "Player"
		verb
			Xray_On()
				see_invisible = 1
			Xray_Off()
				see_invisible = 0
		Click()
			if (see_invisible == 0)
				Xray_On()
			else
				Xray_Off()
client
	show_verb_panel = 0


turf
	icon = 'mapIcons.dmi'
	Grass
		icon_state = "Grass"
		verb
			Dig()
				set src in view(1)
				icon_state = "Grass_digged"
				src.verbs -= /turf/Grass/verb/Dig
				var/obj/Treasure/T = locate()
				if (T.x == src.x && T.y == src.y)
					T.invisibility = 0

				var/obj/Hint/H
				for(H in world)
					if (H && H.x == src.x && H.y == src.y)
						H.invisibility = 0
				var/obj/Enemy/E
				for(E in world)
					if (E && E.x == src.x && E.y == src.y)
						E.invisibility = 0
						E.Fight()
		Click()
			if (abs(x - usr.x) < 2 && abs(y - usr.y) < 2)
				Dig()

	Wall
		icon_state = "Wall"
		density = 1
		opacity = 1

obj

	icon = 'mapIcons.dmi'
	Treasure
		icon_state = "Treasure"
		Click()
			if (abs(x - usr.x) < 2 && abs(y - usr.y) < 2)
				Open()
		verb
			Open()
				set src in view(1)
				alert("You won")
				world << "You won"
				del usr
	Hint
		icon_state = "Hint"
		Click()
			if (abs(x - usr.x) < 2 && abs(y - usr.y) < 2)
				ReadHint()
		verb
			ReadHint()
				set src in view(1)
				var/text1 = "You can find the treasure in the row "
				var/text2 = ", starting counting from bottom."
				var/obj/Treasure/T = locate()
				if (T)
					var/text = text1 + num2text(T.y) + text2
					world << text
				else
					world << "E' tutto uno scam"
				del src
	Enemy
		icon_state = "Enemy"
		verb
			Fight()
				var/power = pick(prob(50); 1, prob(20); 2, prob(15); 3, prob(10); 4, prob(5); 5)
				alert("You fight against a enemy of level "+num2text(power))
				if (prob(power*5) == 1)
					alert("You lost")
					world << "You lost"
					del usr
				else
					alert("You defeated the enemy")
					world << "You defeated the enemy"
					del src
