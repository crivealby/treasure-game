mob
	proc
		AskDimensions()
			var/list/L = new()
			L.Add(input(usr, "Choose the dimension of the squared map.\nMin Rows: 3", "Map Dimension: Rows", 10) as num)
			L.Add(input(usr, "Choose the dimension of the map\nMin Columns: 3", "Map Dimension: Columns", 10) as num)
			if (L[1] < 3)	L[1] = 3
			if (L[2] < 3)	L[2] = 3
			return L
	proc
		GenerateBasicMap(list/dim)
			var/list/Grid[dim[1]][dim[2]]
			var/i
			var/j
			for (i = 1, i < dim[1] + 1, i++)
				for (j = 1, j < dim[2] + 1, j++)
					var/turf/Grass/G = new(locate(j, i, 1))
					Grid[i][j] = 0
			for (i = 1, i < dim[1] + 1, i++)
				for (j = 1, j < dim[2] + 1, j++)
					if (i == 1 || j == 1 || i == dim[1] || j == dim[2])
						var/turf/Wall/W = new(locate(j, i, 1))
						Grid[i][j] = 1
			return Grid
	proc
		listRows(list/grid)
			var/list/rows = new()
			var/c
			for (c = 1, c < grid.len + 1, c++)
				rows.Add(c)
			return rows
	proc
		listCols(list/grid)
			var/list/cols = new()
			var/c
			for (c = 1, c < grid[1].len + 1, c++)
				cols.Add(c)
			return cols
	proc
		RndTreasure(list/grid, list/rows, list/cols)
			var/i = 1
			while (i)
				var/rRow = pick(rows)
				var/rCol = pick(cols)
				if (grid[rRow][rCol] == 0)
					grid[rRow][rCol] = 3
					var/obj/Treasure/T = new(locate(rCol, rRow, 1))
					i--
					T.invisibility = 1

	proc
		RndPlayer(list/grid, list/rows, list/cols)
			var/i = 1
			while (i)
				var/rRow = pick(rows)
				var/rCol = pick(cols)
				if (grid[rRow][rCol] == 0)
					usr.loc = locate(rCol, rRow, 1)
					i--
	proc
		RndHints(list/grid, list/rows, list/cols, i)
			if (i < 2)
				i = 1
			while (i)
				var/rRow = pick(rows)
				var/rCol = pick(cols)
				if (grid[rRow][rCol] == 0)
					grid[rRow][rCol] = 4
					var/obj/Hint/H = new(locate(rCol, rRow, 1))
					i--
					H.invisibility = 1
	proc
		RndEnemy(list/grid, list/rows, list/cols, i)
			if (i < 2)
				i = 1
			while (i)
				var/rRow = pick(rows)
				var/rCol = pick(cols)
				if (grid[rRow][rCol] == 0)
					grid[rRow][rCol] = 5
					var/obj/Enemy/E = new(locate(rCol, rRow, 1))
					i--
					E.invisibility = 1

	proc
		SetupMap(list/grid, n1, n2)
			var/list/dim = AskDimensions()
			grid = GenerateBasicMap(dim)
			var/list/rows = listRows(grid)
			var/list/cols = listCols(grid)
			RndTreasure(grid, rows, cols)
			RndPlayer(grid, rows, cols)
			RndHints(grid, rows, cols, n1)
			RndEnemy(grid, rows, cols, n2)




